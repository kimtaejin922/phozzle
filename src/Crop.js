import React, { Component } from 'react';
import Cropper from 'cropperjs'
import 'cropperjs/dist/cropper.css';

var imageStyle = {
  maxWidth: '100%'
}

var hiddenStyle = {
  display: 'none'
}

/*
1. fileを読み込む
2. 正方形にcropする
3. 4分割する
*/

class Crop extends Component {
  constructor(props){
    super(props);
    this.state = ({
      cropper: {},
      whole: {},
      upperLeft: {},
      upperRight: {},
      lowerLeft: {},
      lowerRight: {},
      imgSrcs: [{}, {}, {}, {}]
    })
    this.imageRef = React.createRef();
    this.previewRef = React.createRef();
    this.upperLeftRef = React.createRef();
    this.upperRightRef = React.createRef();
  }

  handleFileChange(e){
    this.imageRef.current.src = URL.createObjectURL(e.target.files[0]);
    const cropper = new Cropper(this.imageRef.current, {
      viewMode: 1,
      aspectRatio: 1 / 1,
      crop(event) {
      }
    });
    this.setState({cropper});

  }

  handleCropBox(e){
    // console.log(this.state.cropper.getCroppedCanvas());
    // this.state.cropper.setCropBoxData({
    //   width: (this.state.cropper.getCroppedCanvas().width/20),
    //   left: 10,
    //   top: 10});
    console.log(this.state.whole);
  }

  cropImage() {
    this.setState({whole: this.state.cropper.getCroppedCanvas()},
    () => {
      this.previewRef.current.src = this.state.whole.toDataURL('image/jpeg');
      this.state.cropper.destroy();
      var element = document.getElementById("image"); element.parentNode.removeChild(element);
      this.handlePreview();
    })
  }

  handlePreview() {
    this.previewRef.current.src = this.state.whole.toDataURL('image/jpeg');
    const setUpperLeft = (cropper) => {
      this.upperLeftRef.current.src = cropper.getCroppedCanvas().toDataURL('image/jpeg');
      this.upperLeftRef.current.style.width = '50%';
    }

    const setUpperRight = (cropper) => {
      this.upperRightRef.current.src = cropper.getCroppedCanvas().toDataURL('image/jpeg');
      this.upperRightRef.current.style.width = '50%';
    }

    let imgSrcs = this.state.imgSrcs.map((src) => {
        return new Cropper(this.previewRef.current, {
          viewMode: 2,
          ready() {
            this.cropper.setCropBoxData({
              width: this.cropper.getCanvasData().width / 2,
              height: this.cropper.getCanvasData().height / 2,
              left: this.cropper.getCanvasData().left,
              top: this.cropper.getCanvasData().top
            });
            this.cropper.crop();
          }
        });
      })
    console.log(imgSrcs);
    this.setState({imgSrcs})
  }

  handleImgSrcs(pcs, originalSrc){
    const sizePerPcs =  originalSrc.getCanvasData() / Math.sqrt(pcs);
  }

  render() {
    return (
      <div className="App">
        <input type="file" onChange={(e)=>this.handleFileChange(e)}/>
        <img style={imageStyle} id="image" ref={this.imageRef} src='' />
        <div style={hiddenStyle}>
          <img id="preview" ref={this.previewRef} src='' />
        </div>
        {this.state.imgSrcs.map((imgSrc, index)=>{
          return <PhozzlePiece key={index} imgSrc={imgSrc}/>
        })}
        <button onClick={()=>this.cropImage()} >crop</button>
        <button onClick={()=>this.handlePreview()} >piece</button>
        <input onChange={(e)=>this.handleCropBox(e)} />
      </div>
    );
  }
}

class PhozzlePiece extends Component {
  constructor(props){
    super(props);
    this.state = ({
      id: 0
    })
  }

  getSrc(){
    return this.props.imgSrc.getCroppedCanvas().toDataURL('image/jpeg');
  }

  render() {
    return(
      <div>
        <img style={imageStyle} src={this.props.imgSrc.url} />
      </div>
    )
  }
}


export default Crop;
