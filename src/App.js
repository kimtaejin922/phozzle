import React, { Component } from 'react';
import Cropper from 'cropperjs'
import 'cropperjs/dist/cropper.css';
import PuzzleImage from './puzzle.svg';

var imageStyle = {
  maxWidth: '100%'
}

var hiddenStyle = {
  display: 'none'
}

class App extends Component {
  constructor(props) {
    super(props)
    this.state = ({
      page: 'file',
      imgFile: '',
      croppedSrc: ''
    })
  }

  componentDidMount() {

  }

  handlePage = page => {
    this.setState({ page });
  }

  handleFile = e => {
    this.setState({ imgFile: e.target.files[0] });
    this.handlePage('crop');
  }

  handleCroppedSrc = croppedSrc => {
    this.setState({ croppedSrc });
    this.handlePage('puzzle');
  }

  render() {
    return (
      <div>
        {this.state.page === 'file' && <FilePage handleFile={this.handleFile} />}

        {this.state.page === 'crop' && <CropPage
          imgFile={this.state.imgFile} handleCroppedSrc={this.handleCroppedSrc} />}

        {this.state.page === 'puzzle' && <PuzzlePage croppedSrc={this.state.croppedSrc} />}
      </div>
    )
  }
}

const FilePage = props => {
  return (
    <div className="container">
      <img src={PuzzleImage} />
      <div className="title">
        <h2>Phozzle</h2>
      </div>
      <div className="content">
        <label htmlFor="file-input">
          <h3 className="mb-1">사진을 선택해 주세요</h3>
        </label>
        <input id="file-input" type="file" onChange={(e) => props.handleFile(e)} />
      </div>
    </div>
  )
}

class CropPage extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      cropper: {}
    })
  }

  componentDidMount() {
    document.getElementById('originalImg').src = URL.createObjectURL(this.props.imgFile);
    const cropper = new Cropper(document.getElementById('originalImg'), {
      viewMode: 1,
      aspectRatio: 1 / 1,
      crop(event) {
      }
    });
    this.setState({ cropper });
  }

  handleCrop() {
    const dataUrl = this.state.cropper.getCroppedCanvas().toDataURL('image/jpeg');
    this.props.handleCroppedSrc(dataUrl);
  }

  render() {
    return (
      <div>
        <img id='originalImg' style={imageStyle} src='' />

        <h3 className="mb-1">퍼즐로 사용할 부분을 지정해 주세요.</h3>
        <button onClick={() => this.handleCrop()}>지정하기</button>
      </div>)
  }
}

class PuzzlePage extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      numberOfPieces: 9,
      originalWidth: 0,
      croppedSrc: null,
      puzzle: [],
      selected: null
    })
  }

  componentDidMount() {
    const croppedSrc = document.getElementById('croppedSrc');
    setTimeout(() => this.setState({ croppedSrc }), 500);
    this.shuffleThePuzzle();
  }

  shuffleThePuzzle() {
    let puzzle = [];
    let shuffled = shuffle([0, 1, 2, 3, 4, 5, 6, 7, 8]);
    for (let i = 0; i < 3; i++) {
      let row = [];
      for (let j = 0; j < 3; j++) {
        row[j] = shuffled[3 * i + j];
      }
      puzzle[i] = row;
    }
    this.setState({ puzzle });
  }

  handleSelected(selected) {
    this.setState({ selected });
    // console.log(selected);
  }

  handleScroll(e) {
    if (this.state.selected !== null) {
      if ((e.touches[0].target.offsetLeft + (e.touches[0].target.clientWidth / 2) - e.touches[0].clientX) > e.touches[0].target.clientWidth) {
        this.moveSelected('left');
      } else if ((e.touches[0].target.offsetLeft + (e.touches[0].target.clientWidth / 2) - e.touches[0].clientX) <= - e.touches[0].target.clientWidth) {
        this.moveSelected('right');
      }
      if ((e.touches[0].target.offsetTop + (e.touches[0].target.clientHeight / 2) - e.touches[0].clientY) > e.touches[0].target.clientHeight) {
        // console.log('up')
        this.moveSelected('up');
      } else if ((e.touches[0].target.offsetTop + (e.touches[0].target.clientHeight / 2) - e.touches[0].clientY) <= - e.touches[0].target.clientHeight) {
        this.moveSelected('down');
      }

    }
    // console.log("scroll")
  }

  moveSelected(moveTo) {
    if (moveTo === 'left') {
      if (![0, 3, 6].includes(this.state.selected)) {
        this.handlePuzzle(0, -1);
      }
    } else if (moveTo === 'right') {
      if (![2, 5, 8].includes(this.state.selected)) {
        this.handlePuzzle(0, 1);
      }
    } else if (moveTo === 'up') {
      if (![0, 1, 2].includes(this.state.selected)) {
        this.handlePuzzle(-1, 0);
      }
    } else if (moveTo === 'down') {
      if (![7, 8, 9].includes(this.state.selected)) {
        this.handlePuzzle(1, 0);
      }
    }
  }

  handlePuzzle(offsetX, offsetY) {
    let puzzle_before = this.state.puzzle;
    const row = parseInt(this.state.selected / 3);
    const col = this.state.selected % 3;
    // console.log(this.state.selected, row, col)
    let tmp = puzzle_before[row + offsetX][col + offsetY];
    if (tmp === 0) {
      puzzle_before[row + offsetX][col + offsetY] = puzzle_before[row][col];
      puzzle_before[row][col] = tmp;
      this.setState({ puzzle: puzzle_before, selected: null });
      
      let result = puzzle_before.every((r, i) => {
        return r.every((c, j) => {
          return c === j + (i*3);
        });
      });
      
      if (result) alert("퍼즐을 전부 맞추셨습니다. 축하합니다.")
    }
  }

  render() {
    return (
      <>
        <div className="puzzle-wrapper">
          <img id="croppedSrc" src={this.props.croppedSrc} className="hidden" />
          {this.state.puzzle.map((row, i) => {
            return <PuzzleRow key={i} row={row} rowIndex={Number(i)}
              croppedSrc={this.state.croppedSrc}
              handleSelected={(e) => this.handleSelected(e)}
              handleScroll={e => this.handleScroll(e)} />
          })
          }
        </div>
        <button onClick={e => this.shuffleThePuzzle()}>섞기</button>
      </>
    )
  }
}

const PuzzleRow = props => {
  return (props.row.map((col, j) => {
    return <PuzzlePiece key={props.rowIndex * 3 + j} id={props.rowIndex * 3 + j}
      rowIndex={props.rowIndex} j={Number(j)} croppedSrc={props.croppedSrc}
      handleSelected={(e) => props.handleSelected(e)}
      handleScroll={(e) => props.handleScroll(e)}
      col={col}
    />
  }))
}

class PuzzlePiece extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      width: 300
    })
  }

  componentDidUpdate() {
    this.drawImage();
  }

  drawImage() {
    const src = document.getElementById('croppedSrc');
    const id = this.props.col;
    const originalWidth = src.width
    const targetWidth = originalWidth / 3;
    const sx = id % 3 * targetWidth;
    const sy = parseInt(id / 3) * targetWidth;
    var ctx = document.getElementById('canvas' + this.props.col).getContext('2d');
    if (this.props.col === 0) {
      ctx.clearRect(0, 0, targetWidth, targetWidth);
    } else {
      ctx.drawImage(src,
        sx, sy, targetWidth, targetWidth, 0, 0, targetWidth, targetWidth);
    }
    // this.setState({width: targetWidth});
  }

  touchStart() {
    // console.log("touch");
  }

  render() {
    return (
      <canvas id={"canvas" + this.props.col} onClick={() => this.drawImage()} width={this.props.croppedSrc ? this.props.croppedSrc.width / 3 : 0} height={this.props.croppedSrc ? this.props.croppedSrc.width / 3 : 0} col={this.props.col} onTouchStart={() => this.props.handleSelected(Number(this.props.rowIndex * 3 + this.props.j))}
        onTouchEnd={() => this.props.handleSelected(null)}
        onTouchMove={(e) => { this.props.handleScroll(e) }}></canvas>
    )
  }
}

var shuffle = function (array) {

  var currentIndex = array.length;
  var temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;

};

export default App;
