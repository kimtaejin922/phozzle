import React, { Component } from 'react';
import './App.css';

class Puzzle extends Component {
  constructor(props){
    super(props);
    this.state=({
      puzzle: [],
      selected: null
    })
  }

  componentDidMount(){
    let puzzle = [];
    let shuffled = shuffle([0, 1, 2, 3, 4, 5, 6, 7, 8]);
    for(let i = 0; i < 3; i++){
      let row = [];
      for(let j = 0; j < 3; j++){
        row[j] = shuffled[3 * i + j];
      }
      puzzle[i] = row;
    }
    this.setState({puzzle});
    // window.addEventListener('scroll', this.handleScroll);
  }

  handleSelected(col){
    this.setState({selected: col});
  }

  handleScroll(e){
    if(this.state.selected !== null){
      if((e.touches[0].target.offsetLeft + (e.touches[0].target.clientWidth/2) - e.touches[0].clientX) > e.touches[0].target.clientWidth){
        this.moveSelected('left');
      } else if ((e.touches[0].target.offsetLeft + (e.touches[0].target.clientWidth/2) - e.touches[0].clientX) <=  - e.touches[0].target.clientWidth){
        this.moveSelected('right');
      }
      if((e.touches[0].target.offsetParent.offsetTop + (e.touches[0].target.clientHeight/2) - e.touches[0].clientY) > e.touches[0].target.clientHeight){
        console.log('up')
        this.moveSelected('up');
      } else if ((e.touches[0].target.offsetParent.offsetTop + (e.touches[0].target.clientHeight/2) - e.touches[0].clientY) <= - e.touches[0].target.clientHeight) {
        this.moveSelected('down');
      }

    }
  }

  moveSelected(moveTo){
    if(moveTo === 'left'){
      if(![0, 3, 6].includes(this.state.selected)){
        this.handlePuzzle(0, -1);
      }
    } else if(moveTo === 'right'){
      if(![2, 5, 8].includes(this.state.selected)){
        this.handlePuzzle(0, 1);
      }
    } else if(moveTo === 'up'){
      if(![0, 1, 2].includes(this.state.selected)){
        this.handlePuzzle(-1, 0);
      }
    } else if(moveTo === 'down'){
      if(![7, 8, 9].includes(this.state.selected)){
        this.handlePuzzle(1, 0);
      }
    }
  }

  handlePuzzle(offsetX, offsetY){
    let puzzle_before = this.state.puzzle;
    const row = parseInt(this.state.selected / 3);
    const col = this.state.selected % 3;
    // console.log(this.state.selected, row, col)
    let tmp = puzzle_before[row + offsetX][col + offsetY];
    if(tmp === 0){
      puzzle_before[row + offsetX][col + offsetY] = puzzle_before[row][col];
      puzzle_before[row][col] = tmp;
      this.setState({puzzle: puzzle_before, selected: null});
    }
  }

  render(){
    return(
      this.state.puzzle.map((row, i)=>{
        return <table><tr><PuzzleRow key={i} row={row} rowIndex={i} handleSelected={(e)=>this.handleSelected(e)}
        handleScroll={e=>this.handleScroll(e)}/></tr></table>
      })
    )
  }
}

const PuzzleRow = props => {
  return (props.row.map((col, j)=>{
    return <td key={j} onTouchStart={()=>props.handleSelected(props.rowIndex*3+j)}
    onTouchEnd={()=>props.handleSelected(null)}
    onTouchMove={(e)=>{props.handleScroll(e)}}>{col === 0 || col}</td>
  }))
}

var shuffle = function (array) {

	var currentIndex = array.length;
	var temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;

};

export default Puzzle;
