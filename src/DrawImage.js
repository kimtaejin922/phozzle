import React, { Component } from 'react';

class DrawImage extends Component {
  componentDidMount(){
    console.log("mounted")
    var ctx = document.getElementById('canvas').getContext('2d');
    ctx.drawImage(document.getElementById('source'),
                  0, 0, 100, 100, 0, 0, 100, 100);
    var ctx2 = document.getElementById('canvas2').getContext('2d');
    ctx2.drawImage(document.getElementById('source'),
                  100, 0, 100, 100, 0, 0, 100, 100);
    // Draw frame
    // ctx.drawImage(document.getElementById('frame'), 0, 0);
  }
  render(){
    return(
      <div>
        <div>
          <canvas id="canvas" width="100" height="100"></canvas>
          <canvas id="canvas2" width="100" height="100"></canvas>
          <canvas id="canvas3" width="100" height="100"></canvas>
        </div>
        <img id="source" src="https://mdn.mozillademos.org/files/5397/rhino.jpg" width="300" height="300" />
      </div>
    )
  }
}

export default DrawImage;
